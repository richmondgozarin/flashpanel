from ferris import  Controller, scaffold, messages, route, route_with
from ferris.components.pagination import Pagination
from app.models.order import Order
from google.appengine.api import users

import logging

class Orders(Controller):
    class Meta:
        prefixes = ('admin',)
        components = (scaffold.Scaffolding, Pagination)
        Model = Order
        pagination_limit = 10

    class Scaffold:
        display_properties = ('customer', 'current_seats', 'additional_seats', 'currency', 'created_by', 'created')

    admin_add = scaffold.add
    admin_list = scaffold.list
    admin_delete = scaffold.delete
    admin_edit = scaffold.edit

    @route_with(template='/')
    def show(self):
        user = users.get_current_user()

        if user:
            self.context['user'] = 'Welcome %s!' % user.nickname()
            if users.is_current_user_admin():
                self.context['admin'] = 'Admin here'
        else:
            self.context['user'] = 'User is not yet log in.'


