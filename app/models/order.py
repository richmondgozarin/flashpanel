from ferris import BasicModel, ndb
from ferris.behaviors import searchable

class Order(BasicModel):

    class Meta:
        behaviors = (searchable.Searchable,)

    limits = []

    for x in xrange(25,51):
        limits.append(x)

    customer = ndb.StringProperty(indexed=True, required=True)
    current_seats = ndb.IntegerProperty(required=True)
    additional_seats = ndb.IntegerProperty(required=True, choices=(limits) , default=25)
    currency = ndb.StringProperty(choices=('USD', 'PHP',), default='USD', required=True)


    @classmethod
    def show_all(cls):
        return cls.query().order(cls.customer)

    @classmethod
    def create(cls, **params):
        try:
            item = cls(**params)
            item.put()
            return item
        except Exception as e:
            print e
            return None
